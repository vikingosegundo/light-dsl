
func newLight(withId id: String, andName name: String) -> Light { Light(id: id, name: name) }

let l = alter(newLight(withId:"01", andName:"01"),
               by:
                 .setting (.hue       (to:0.1      )),
                 .setting (.saturation(to:0.75     )),
                 .setting (.brightness(to:0.5      )),
                 .setting (.ct        (to:200.mirek)),
                 .renaming(.it        (to:"light 1")),
                 .turning (.it        (.on         )),
                 .toggling(.display   (to:.scrubbing))
)

// let l be: alter new light with id "01" and name "01"
//                   by
//                    * setting hue to 0.1,
//                    * setting saturation to 0.75,
//                    * setting brightness to 0.5,
//                    * setting color temperatur to 200,
//                    * renaming it to "light 1"
//                    * turning it on
//                    * toggling display to scrubbing


assert(l.id         == "01"     )
assert(l.hue        == 0.1      )
assert(l.saturation == 0.75     )
assert(l.brightness == 0.5      )
assert(l.name       == "light 1")
assert(l.isOn       == true     )
print(l)
