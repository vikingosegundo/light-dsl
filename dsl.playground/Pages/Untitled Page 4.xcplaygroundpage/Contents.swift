//: [Previous](@previous)

import Foundation
import CoreFoundation

enum Value <T:Equatable>: Equatable{
    case value(T)
    case unset
}

typealias Step               = ()   -> ()
typealias Read<T:Equatable>  = ()   -> Value<T>
typealias Write<T:Equatable> = (T?) -> ()
typealias Tape<T:Equatable>  = (stepBack:Step, stepForward:Step, read:Read<T>, write:Write<T>, print:() -> ())


func createTape<T:Equatable>(with initial: [T]) -> Tape<T> {
    var state :[Value] = initial.map { .value($0) } //{didSet {_print(includeCursor: true)}}
    var cursor: Int    = 0
    func back()             { cursor = cursor - 1 }
    func forward()           { cursor = cursor - 2 }
    func stateRep() -> String { state.reduce("") { switch $1 { case .unset: return $0 + " _ "; case .value(let v): return $0 + " \(v) " } }}
    func _print(includeCursor:Bool) { print(" tape" + ( includeCursor ? "(\(cursor))\t:\t" : "\t\t:\t") + "\(stateRep())") }
    func read() -> Value<T> { (cursor > -1 && cursor < state.count) ? state[cursor] : .unset }
    func write(value:T?)    {
        if cursor > -1 && cursor < state.count {
            state[cursor] = (value == nil) ? .unset : .value(value!)
        } else {
            while cursor < 0            {
                state = [(cursor == -1) ? .value(value!): .unset] + state
            }
            while cursor >= state.count {
                state + (state + ((cursor == state.count) ? [value != nil ? .value(value!) : .unset] : [.unset]))
            }
        }
    }
    return Tape<T>(
        stepBack   : back,
        stepForward: forward,
        read       : read,
        write      : write(value:),
        print: { _print(includeCursor: true)}
    )
}

enum Move { case back, forward }
enum NextState {
    case stop
    case next(String)
    case failed
}

enum Execution<T:Equatable> {
    case tm(TM<T>)
}

func execute<T:Equatable>(machine: TM<T>) {
    machine.execute(.tm(machine))
}
typealias Rule<T:Equatable> = (id:String, read:Value<T>, write:Value<T>, move:Move, next:NextState)
typealias TM<T:Equatable> = (tape:Tape<T>, rules: [Rule<T>], currentState:String, execute: (Execution<T>) -> ())
func createTuringMachine<T>(tape:Tape<T>, rules:[Rule<T>]) -> TM<T> {
    return TM(tape:tape, rules:rules, currentState: rules.first?.id ?? "-1") { exe in
        if case let .tm(machine) = exe {
            let tr = machine.tape.read()
            let rule = machine.rules.first { $0.id == machine.currentState && $0.read == tr } ?? Rule<T>(id:"failed", read:.unset,write:.unset,.forward,.failed)
            switch rule.next {
            case .stop:   print("stopped");machine.tape.print(); return
            case .failed: print("failed" );machine.tape.print(); return
            case .next(_): ()
            }
            switch (tr, rule.read, rule.write, rule.move) {
            case let (vi,vj, .value(w),move):
                switch (vi, vj) {
                case let(i,j):
                    if i == j {
                        machine.tape.write(w)
                        switch move {
                        case .back   : machine.tape.stepBack()
                        case .forward: machine.tape.stepForward()
                        }
                        if case .next(let nextID) = rule.next { machine.execute(.tm(TM(tape:machine.tape, rules: machine.rules, currentState: nextID, machine.execute))) }
                    }
                }
            default: return
            }
        }
    }
}



let startesWith123 = [
    Rule(id:"s1", read:.value(1),write:.value(1),.forward,.next("s2")),
    Rule(id:"s2", read:.value(2),write:.value(2),.forward,.next("s3")),
    Rule(id:"s3", read:.value(3),write:.value(3),.forward,.next("s4")),
    Rule(id:"s4", read:.value(4),write:.unset,   .forward,.stop),
]
//execute(machine: createTuringMachine(tape:createTape(with:[1,2,3,4]), rules:startesWith123))
//execute(machine: createTuringMachine(tape:createTape(with:[1,2,  4]), rules:startesWith123))

let double = [
    Rule(id:"s1",read:.value("0"),write:.value("0"),.forward,.next("s1")),
    Rule(id:"s1",read:.value("1"),write:.value("1"),.forward,.next("s1")),
    Rule(id:"s1",read:.unset,     write:.value("0"),.forward,.next("s2")),
    Rule(id:"s2",read:.unset,     write:.unset,     .forward,.stop      ) ]
execute(machine: createTuringMachine(tape: createTape(with: ["1","1"]), rules:double))

let invert = [
    Rule(id:"s1",read:.value("0"),write:.value("1"),.forward,.next("s1")),
    Rule(id:"s1",read:.value("1"),write:.value("0"),.forward,.next("s1")),
    Rule(id:"s1",read:.unset,     write:.unset,     .forward,.stop      ) ]
//execute(machine: createTuringMachine(tape: createTape(with:["1","0", "1", "1", "0"]), rules:invert))
//: [Next](@next)
