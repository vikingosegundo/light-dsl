
public
func alter(_ light:Light, by changes: Light.Change...) -> Light { changes.reduce(light) { $0.alter($1) } }

public
struct Light: Codable, Equatable, Identifiable {
    public init(id: String, name: String) {
        self.init(id, name, false, 0, 0, 0, 0, .slider, [])
    }
    public enum Change {
        case renaming(_RenameIt)
        case turning (_TurnIt  )
        case adding  (_Add     )
        case toggling(_Toggle  )
        case setting (_Set     )

        public enum _Add    { case mode   (Mode)         }
        public enum _Toggle { case display(to:Interface) }
        public enum _Set    {
            case hue       (to:Double)
            case saturation(to:Double)
            case brightness(to:Double)
            case ct        (to:CT    )
        }
        public enum _RenameIt { case it(to:String) }
        public enum _TurnIt   { case it(Turn) }
    }
    public
    enum Mode:Codable { case hsb, ct }
    public let id        : String
    public let name      : String
    public let isOn      : Bool

    public let hue       : Double
    public let saturation: Double
    public let brightness: Double
    public let ct        : Int

    public let modes     : [Mode]
    public let display   : Interface

    private init(_ id: String, _ name: String, _ isOn: Bool, _ brightness: Double, _ saturation: Double, _ hue: Double, _ ct: Int, _ display: Interface, _ modes: [Mode]) {
        self.id         = id
        self.name       = name
        self.isOn       = isOn
        self.hue        = hue
        self.saturation = saturation
        self.brightness = brightness
        self.ct         = checkCT(ct)
        self.modes      = modes
        self.display    = display
    }

    func alter(_ changes: Change... ) -> Self { changes.reduce(self) { $0.alter($1) } }

    private func alter( _ change: Change) -> Self {
        func __(_ x: Double) -> Double { max(min(x, 1.0), 0.0) }
        switch change {
        case let .renaming(        .it(to:name      )): return .init(id, name, isOn,       brightness,   saturation,   hue,  ct, display, modes         )
        case let .turning (        .it(isOn         )): return .init(id, name, isOn == .on,brightness,   saturation,   hue,  ct, display, modes         )
        case let .setting (.brightness(to:brightness)): return .init(id, name, isOn,    __(brightness),  saturation,   hue,  ct, display, modes         )
        case let .setting (.saturation(to:saturation)): return .init(id, name, isOn,       brightness,__(saturation),  hue,  ct, display, modes         )
        case let .setting (       .hue(to:hue       )): return .init(id, name, isOn,       brightness,   saturation,__(hue), ct, display, modes         )
        case let .setting (       .ct (to:.mirek(ct))): return .init(id, name, isOn,       brightness,   saturation,   hue,  ct, display, modes         )
        case let .toggling(   .display(to:display   )): return .init(id, name, isOn,       brightness,   saturation,   hue,  ct, display, modes         )
        case let .adding  (      .mode(mode         )): return .init(id, name, isOn,       brightness,   saturation,   hue,  ct, display, modes + [mode])
        }
    }
}


// Common Lighting DSL Elements
public enum Values     { case hsb(Double, Double, Double), ct(Int, Double), bri(Double) }
public enum Items      { case lights                                                    }
public enum LightValue { case hue, saturation, brightness, colortemp                    }
public enum Outcome    { case succeeded, failed(Error?)                                 }
public enum Increment  { case pt(Double)                                                }
public enum Turn       { case on, off                                                   }
public enum Apply      { case values(Values)                                            }
public enum Alter      { case name(String) , display(Interface)                         }
public enum CT         { case mirek(Int)                                                }

public enum Interface: Codable  { case scrubbing, slider }

public extension Double { var pt   : Increment { .pt   (self) } }
public extension Int    { var mirek: CT        { .mirek(self) } }

private
func checkCT(_ ct: Int) -> Int {
    switch ct {
    case (153..<501    ): return ct
    case (501...Int.max): return 500
    default: return 153
    }
}
