```Swift
let l = alter(newLight(withId:"01", andName:"01"),
               by:
                 .setting (.hue       (to:0.1      )),
                 .setting (.saturation(to:0.75     )),
                 .setting (.brightness(to:0.5      )),
                 .setting (.ct        (to:200.mirek)),
                 .renaming(.it        (to:"light 1")),
                 .turning (.it        (.on         ))
)

```
    let l be: alter new light with id "01" and name "01"
                  by
                   * setting hue to 0.1,
                   * setting brightness to 0.5,
                   * setting saturation to 0.75,
                   * setting color temperatur to 200 mirek,
                   * renaming it to "light 1"
                   * turning it on
